﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_Tareas_de_Among_Us
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea11.Visible = radioButton1.Checked;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea21.Visible = radioButton2.Checked;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea31.Visible = radioButton3.Checked;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea41.Visible = radioButton4.Checked;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea51.Visible = radioButton5.Checked;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            ucTarea61.Visible = radioButton6.Checked;
        }

        private void rbtnHome_CheckedChanged(object sender, EventArgs e)
        {
            panelHome.Visible = rbtnHome.Checked;
        }
    }
}
