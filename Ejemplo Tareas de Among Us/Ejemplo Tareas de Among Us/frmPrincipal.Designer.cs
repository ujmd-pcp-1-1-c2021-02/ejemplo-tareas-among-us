﻿
namespace Ejemplo_Tareas_de_Among_Us
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.ucTarea61 = new Ejemplo_Tareas_de_Among_Us.ucTarea6();
            this.ucTarea51 = new Ejemplo_Tareas_de_Among_Us.ucTarea5();
            this.ucTarea41 = new Ejemplo_Tareas_de_Among_Us.ucTarea4();
            this.ucTarea31 = new Ejemplo_Tareas_de_Among_Us.ucTarea3();
            this.ucTarea21 = new Ejemplo_Tareas_de_Among_Us.ucTarea2();
            this.ucTarea11 = new Ejemplo_Tareas_de_Among_Us.ucTarea1();
            this.rbtnHome = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelHome = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.panelPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radioButton1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radioButton2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radioButton3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radioButton4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radioButton5, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radioButton6, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panelPrincipal, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtnHome, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 480);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // radioButton1
            // 
            this.radioButton1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton1.FlatAppearance.BorderSize = 2;
            this.radioButton1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.radioButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.radioButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(3, 193);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(144, 34);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.Text = "Tarea 1";
            this.radioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton2.FlatAppearance.BorderSize = 2;
            this.radioButton2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.radioButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.radioButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(3, 233);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(144, 34);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Tarea 2";
            this.radioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton3.FlatAppearance.BorderSize = 2;
            this.radioButton3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.radioButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.radioButton3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.Location = new System.Drawing.Point(3, 273);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(144, 34);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.Text = "Tarea 3";
            this.radioButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton4.FlatAppearance.BorderSize = 2;
            this.radioButton4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.radioButton4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.radioButton4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.Location = new System.Drawing.Point(3, 313);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(144, 34);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.Text = "Tarea 4";
            this.radioButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton5.FlatAppearance.BorderSize = 2;
            this.radioButton5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.radioButton5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.radioButton5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton5.Location = new System.Drawing.Point(3, 353);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(144, 34);
            this.radioButton5.TabIndex = 5;
            this.radioButton5.Text = "Tarea 5";
            this.radioButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // radioButton6
            // 
            this.radioButton6.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton6.FlatAppearance.BorderSize = 2;
            this.radioButton6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.radioButton6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.radioButton6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.Location = new System.Drawing.Point(3, 393);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(144, 34);
            this.radioButton6.TabIndex = 6;
            this.radioButton6.Text = "Tarea 6";
            this.radioButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelPrincipal.Controls.Add(this.panelHome);
            this.panelPrincipal.Controls.Add(this.ucTarea61);
            this.panelPrincipal.Controls.Add(this.ucTarea51);
            this.panelPrincipal.Controls.Add(this.ucTarea41);
            this.panelPrincipal.Controls.Add(this.ucTarea31);
            this.panelPrincipal.Controls.Add(this.ucTarea21);
            this.panelPrincipal.Controls.Add(this.ucTarea11);
            this.panelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrincipal.Location = new System.Drawing.Point(153, 3);
            this.panelPrincipal.Name = "panelPrincipal";
            this.tableLayoutPanel1.SetRowSpan(this.panelPrincipal, 9);
            this.panelPrincipal.Size = new System.Drawing.Size(644, 474);
            this.panelPrincipal.TabIndex = 2;
            // 
            // ucTarea61
            // 
            this.ucTarea61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea61.Location = new System.Drawing.Point(0, 0);
            this.ucTarea61.Name = "ucTarea61";
            this.ucTarea61.Size = new System.Drawing.Size(644, 474);
            this.ucTarea61.TabIndex = 5;
            this.ucTarea61.Visible = false;
            // 
            // ucTarea51
            // 
            this.ucTarea51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea51.Location = new System.Drawing.Point(0, 0);
            this.ucTarea51.Name = "ucTarea51";
            this.ucTarea51.Size = new System.Drawing.Size(644, 474);
            this.ucTarea51.TabIndex = 4;
            this.ucTarea51.Visible = false;
            // 
            // ucTarea41
            // 
            this.ucTarea41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea41.Location = new System.Drawing.Point(0, 0);
            this.ucTarea41.Name = "ucTarea41";
            this.ucTarea41.Size = new System.Drawing.Size(644, 474);
            this.ucTarea41.TabIndex = 3;
            this.ucTarea41.Visible = false;
            // 
            // ucTarea31
            // 
            this.ucTarea31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea31.Location = new System.Drawing.Point(0, 0);
            this.ucTarea31.Name = "ucTarea31";
            this.ucTarea31.Size = new System.Drawing.Size(644, 474);
            this.ucTarea31.TabIndex = 2;
            this.ucTarea31.Visible = false;
            // 
            // ucTarea21
            // 
            this.ucTarea21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea21.Location = new System.Drawing.Point(0, 0);
            this.ucTarea21.Name = "ucTarea21";
            this.ucTarea21.Size = new System.Drawing.Size(644, 474);
            this.ucTarea21.TabIndex = 1;
            this.ucTarea21.Visible = false;
            // 
            // ucTarea11
            // 
            this.ucTarea11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.ucTarea11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTarea11.ForeColor = System.Drawing.Color.White;
            this.ucTarea11.Location = new System.Drawing.Point(0, 0);
            this.ucTarea11.Name = "ucTarea11";
            this.ucTarea11.Size = new System.Drawing.Size(644, 474);
            this.ucTarea11.TabIndex = 0;
            this.ucTarea11.Visible = false;
            // 
            // rbtnHome
            // 
            this.rbtnHome.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnHome.Checked = true;
            this.rbtnHome.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnHome.FlatAppearance.BorderSize = 2;
            this.rbtnHome.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.rbtnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.rbtnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.rbtnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnHome.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnHome.Location = new System.Drawing.Point(3, 153);
            this.rbtnHome.Name = "rbtnHome";
            this.rbtnHome.Size = new System.Drawing.Size(144, 34);
            this.rbtnHome.TabIndex = 0;
            this.rbtnHome.TabStop = true;
            this.rbtnHome.Text = "Inicio";
            this.rbtnHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnHome.UseVisualStyleBackColor = true;
            this.rbtnHome.CheckedChanged += new System.EventHandler(this.rbtnHome_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Ejemplo_Tareas_de_Among_Us.Properties.Resources.among_us_2073273_stb7;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelHome
            // 
            this.panelHome.BackgroundImage = global::Ejemplo_Tareas_de_Among_Us.Properties.Resources.Among_Us_menu;
            this.panelHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHome.Location = new System.Drawing.Point(0, 0);
            this.panelHome.Name = "panelHome";
            this.panelHome.Size = new System.Drawing.Size(644, 474);
            this.panelHome.TabIndex = 6;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 480);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPrincipal";
            this.Text = "Réplica de Tareas de Among Us";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panelPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rbtnHome;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.Panel panelPrincipal;
        private ucTarea1 ucTarea11;
        private ucTarea6 ucTarea61;
        private ucTarea5 ucTarea51;
        private ucTarea4 ucTarea41;
        private ucTarea3 ucTarea31;
        private ucTarea2 ucTarea21;
        private System.Windows.Forms.Panel panelHome;
    }
}

