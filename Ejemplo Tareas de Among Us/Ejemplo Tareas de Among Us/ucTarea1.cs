﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_Tareas_de_Among_Us
{
    public partial class ucTarea1 : UserControl
    {
        public ucTarea1()
        {
            InitializeComponent();
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            tmrLlenar.Enabled = true;
            panelRojo.BackColor = Color.Red;
        }

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            tmrLlenar.Enabled = false;
            panelRojo.BackColor = Color.FromArgb(32,0,0);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void tmrLlenar_Tick(object sender, EventArgs e)
        {
            if (panelGasolina.Height < panelTanque.Height)
            {
                panelGasolina.Height += 2;
            }
            else
            {
                tmrLlenar.Enabled = false;
                panelRojo.BackColor = Color.FromArgb(32, 0, 0);
                panelVerde.BackColor = Color.Lime;
                lblTarea1.Text = "TAREA 1 COMPLETADA";
                lblTarea1.ForeColor = Color.Lime;
                button1.Enabled = false;
            }
        }

        private void ucTarea1_Load(object sender, EventArgs e)
        {
            panelGasolina.Height = 0;
        }
    }
}
