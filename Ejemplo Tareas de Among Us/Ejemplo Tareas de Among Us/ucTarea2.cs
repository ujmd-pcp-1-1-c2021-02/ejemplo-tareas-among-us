﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_Tareas_de_Among_Us
{
    public partial class ucTarea2 : UserControl
    {
        public ucTarea2()
        {
            InitializeComponent();
        }

        private void bntDownload_Click(object sender, EventArgs e)
        {
            pbxGifDescarga.Visible = true;
            btnDownload.Visible = false;
            progressBar1.Value = 0;
            progressBar1.Visible = true;
            lblPorcentaje.Visible = true;
            lblTiempo.Visible = true;
            tmrDownload.Enabled = true;
        }

        private void tmrDownload_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 70)
            {
                progressBar1.Value += 14;

            }
            else if (progressBar1.Value < 100)
            {
                progressBar1.Value++;
                tmrDownload.Interval = 100;
            }
            else
            {
                tmrDownload.Enabled = false;
            }
            lblPorcentaje.Text = progressBar1.Value + "%";

            switch (progressBar1.Value)
            {
                case 0:
                    lblTiempo.Text = "Estimated Time: 5d 16hr 4m 49s";
                    break;
                case 14:
                    lblTiempo.Text = "Estimated Time: 11hr 25m";
                    break;
                case 28:
                    lblTiempo.Text = "Estimated Time: 3hr 53m";
                    break;
                case 42:
                    lblTiempo.Text = "Estimated Time: 2hr 6m 2s";
                    break;
                case 56:
                    lblTiempo.Text = "Estimated Time: 34m 48s";
                    break;
                case 70:
                    lblTiempo.Text = "Estimated Time: 3s";
                    break;
                case 100:
                    lblTiempo.Text = "Complete";
                    pbxGifDescarga.Visible = false;
                    lblTarea1.Text = "TAREA 2 COMPLETADA";
                    lblTarea1.ForeColor = Color.Lime;
                    break;
                default:
                    int MilisegundosRestantes = (100 - progressBar1.Value) * tmrDownload.Interval;
                    int SegundosRestantes = MilisegundosRestantes / 1000 + 1;
                    lblTiempo.Text = "Estimated Time: " + SegundosRestantes + "s";
                    break;
            }
        }
    }
}
